import path from 'path';
import { promises as fs } from 'fs';

export async function getData(Questions) {
    let fileContents
    const dataDirectory = path.join(process.cwd(), 'Data');
    if (Questions) fileContents = await fs.readFile(dataDirectory + '/questions.json', 'utf8')
    else fileContents = await fs.readFile(dataDirectory + '/answers.json', 'utf8');
    const json = JSON.parse(fileContents)
    return json;
}