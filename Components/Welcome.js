import Router from "next/router";
function Welcome() {
    function startQuestions() {
        Router.push('/Questions')
    }
    return (
        <div id='welcome'>
            <p>For each exam item select the answer that best answers the question. You will do this by moving your cursor to the circle by your answer choice and then left clicking to fill in the circle. Once your choice has been highlighted, click `Next` to go on to the next question.</p>
            <button onClick={startQuestions}>Start</button>

        </div>
    )
}

export default Welcome;