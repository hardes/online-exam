import { useContext } from "react"
import { AppContext } from "../pages/_app"
export default function DisplayResult() {
    const Result = useContext(AppContext)
    return (
        <>
            <h2>Result</h2>
            <div>{`${Result.result.correct} out of ${Result.result.total} is Correct`}</div>
        </>
    )

}
