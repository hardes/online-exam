import '@/styles/globals.css'
import { useState, createContext } from 'react';
export const AppContext = createContext();
export default function App({ Component, pageProps }) {
  const [result, setResult] = useState();
  return (
    <AppContext.Provider value={{ result, setResult }}>
      <h1 className='Title'>Online Exam</h1>
      <Component {...pageProps} />
    </AppContext.Provider>
  )
}
