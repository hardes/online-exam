import { getData } from "@/Server/getData"

export default async function handler(req, res) {
  if (req.method === 'POST') {
    let userAnswersJson = JSON.parse(req.body);
    console.log("🚀 ~ file: submit.js:7 ~ handler ~ userAnswersJson", userAnswersJson)
    let userAnswers = Array.from(userAnswersJson)
    let answersFile = await getData(false);
    let answers = Array.from(answersFile.answers)
    let pass = 0
    let total = 0
    total = answers.length
    for (let count = 0; count < answers.length; count++) {
      if (answers[count] === userAnswers[count]) pass++;
    }
    let result = { correct: pass, total: total }
    res.json(result)
    res.end()
  }
}