import { useState, useContext, useRef } from 'react'
import { getData } from "@/Server/getData";
import { AppContext } from './_app';
import DisplayResult from '@/Components/DisplayResult';
import styles from '@/styles/Home.module.css'
import profilePic from '@/public/vercel.svg'
import Image from 'next/image';
export default function Questions({ Data }) {
    const [count, setCount] = useState(0)
    const [answer, setAnswer] = useState(Data.map(() => ''));
    const Result = useContext(AppContext)
    const [showQuestions, changeShowQuestions] = useState(true)
    const QuestionHighlights = useRef([])
    const CategoryBtns = useRef([])
    const [groupData] = useState(() => {
        let groups = new Set()
        let groupsCount = []
        let countGroup = -1
        let countNum = 0
        Data.forEach((d) => {
            if (!groups.has(d.group)) {
                groups.add(d.group)
                countGroup++
                countNum = 1
            }
            else {
                countNum++
                groupsCount[countGroup] = countNum
            }
        })
        let groupsArr = ["All Sections", ...groups]
        let groupsCountArr = [Data.length, ...groupsCount]
        return { groupsName: [...groupsArr], groupsCount: [...groupsCountArr] }
    })
    const [totalCount, setTotalCount] = useState(groupData.groupsCount[0])
    const setInputToAnswer = (event) => {
        setAnswer((prev) => {
            prev[count] = event.target.value
            return [...prev]
        })
    }
    function setQues(num) {
        setCount((prev) => prev + num)
    }
    function setCategoryCount(index) {
        if (index === 0 || 1) setCount(0)
        if (index === 0) setTotalCount(groupData.groupsCount[index])
        if (index >= 1) {
            let groupsCountArr = groupData.groupsCount.slice(1, index + 1)
            setTotalCount(groupsCountArr.reduce((total, num) => total + num))
        }
        if (index > 1) setCount(groupData.groupsCount[index - 1])
        CategoryBtns.current.forEach((element) => element.style.backgroundColor = "#4d8dcc")
        CategoryBtns.current[index].style.backgroundColor = "blue"
    }

    function submitAnswers() {
        fetch('/api/submit', {
            method: 'POST',
            body: JSON.stringify(answer)
        }).then((res) => res.json())
            .then((d) => Result.setResult(d))
            .then(() => changeShowQuestions(false))
    }
    if (showQuestions) {
        return (
            <section id={styles.ParentSec}>
                <section id={styles.Left}>
                    <div className={styles.QuestionCategories}>
                        {groupData.groupsName.map((Name, index) => {
                            return <button key={Name} onClick={() => setCategoryCount(index)} ref={element => CategoryBtns.current[index] = element}>{Name}</button>
                        })}
                    </div>
                    <div id={styles.QuesBlock}>
                        <div id={styles.quesNum}>{`Ques No: ${count + 1}`}</div>
                        <div id={styles.QuesBody}>
                            <div className={styles.Questions}>
                                <h4>INSTRUCTION</h4>
                                <p>Select the answer that best answers the question. You will do this by moving your cursor to the circle by your answer choice and then left clicking to fill in the circle. Once your choice has been highlighted, click `Next` to go on to the next question.</p>
                            </div>
                            <div className={styles.Questions}>
                                <h4>QUESTION</h4>
                                <p>{Data[count].question}</p>
                                {
                                    Data[count].options.map((data) => {
                                        return <div key={data} className={styles.answerOption}>
                                            <input
                                                type="radio"
                                                value={data}
                                                name={'Question' + count}
                                                onChange={setInputToAnswer}
                                            />
                                            <span>{data}</span>
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div >
                    <div id={styles.BtnBlock}>
                        <button onClick={() => {
                            setAnswer((prev) => {
                                let arr = [...prev]
                                arr[count] = ''
                                return [...arr]
                            })
                        }}>CLEAR RESPONSE</button>
                        {count < totalCount - 1 ? <button onClick={() => {
                            QuestionHighlights.current[count].style.backgroundColor = 'yellow'
                            setQues(1)
                        }}>REVIEW</button> : ''}
                        <button onClick={() => {
                            QuestionHighlights.current[count].style.backgroundColor = 'white'
                            setQues(1)
                        }}>DUMP</button>
                        {count !== 0 ? <button onClick={() => {
                            setQues(-1)
                        }}>Prev</button> : ''}
                        {count < totalCount - 1 ? <button onClick={() => {
                            QuestionHighlights.current[count].style.backgroundColor = 'lightgreen'
                            setQues(1)
                        }}>Next</button> : ''}
                    </div>
                </section>
                <section>
                    <div id={styles.info}>
                        <Image src={profilePic} alt='Profile-Pic' />
                        <h4>Me</h4>
                    </div>
                    <div id={styles.Pallete}>
                        <h4>Question Pallete</h4>
                        {Data.map((d, index) => {
                            return <button key={index} ref={element => QuestionHighlights.current[index] = element}
                                onClick={() => {
                                    setCategoryCount(0)
                                    setCount(index)
                                }}>{index + 1}</button>
                        })}
                    </div>
                    <div id={styles.submitBlock}>
                        <button onClick={submitAnswers}>Submit</button>
                    </div>
                </section>
            </section>
        );
    }
    return <DisplayResult />
}
export async function getServerSideProps() {
    let Data = await getData(true)
    return {
        props: { Data },
    };
}